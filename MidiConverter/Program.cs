﻿using System;
using System.IO;
using System.Linq;
using System.Text;
using Melanchall.DryWetMidi.Core;
using Melanchall.DryWetMidi.Interaction;

namespace MidiConverter
{
    class Program
    {
        static void Main(string[] args)
        {
            const string filepath = "E:\\GitLab\\cpeg222-piano-project\\MidiConverter\\moon.mid";
            const string outfilePattern = "moon";
            Console.WriteLine("Converting MIDI File : " + filepath);
            ConvertMidiToText(filepath, outfilePattern);
        }
        
        public static void ConvertMidiToText(string midiFilePath, string pattern)
        {
            var midiFile = MidiFile.Read(midiFilePath);
            var tempoMap = midiFile.GetTempoMap();

            long lastTime = 0;

            File.WriteAllText(pattern + "_notes.txt", "");
            File.WriteAllText(pattern + "_duration.txt", "");
            File.WriteAllText(pattern + "_times.txt", "");
            
            midiFile.GetNotes().All(n =>
            { 
                File.AppendAllText(pattern + "_notes.txt", $"pr2_midi_preset[{n.NoteNumber}], ", Encoding.UTF8);
                File.AppendAllText(pattern + "_duration.txt", $"{n.LengthAs<MetricTimeSpan>(tempoMap).TotalMicroseconds / 1000}, ", Encoding.UTF8);
                File.AppendAllText(pattern + "_times.txt", $"{n.TimeAs<MetricTimeSpan>(tempoMap).TotalMicroseconds / 1000 - lastTime}, ", Encoding.UTF8);

                lastTime = n.TimeAs<MetricTimeSpan>(tempoMap).TotalMicroseconds / 1000;
                
                return true;
            });
        }
    }
}